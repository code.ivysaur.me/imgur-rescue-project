#!/bin/bash

echo "Current run:"

echo -n "- Downloading:    "
fgrep 'Downloading' ./collect-logs.txt | wc -l

echo -n "- Unsupported:    "
fgrep 'Unsupported' ./collect-logs.txt | wc -l

echo -n "- 404/missing:    "
fgrep 'Failed API lookup' ./collect-logs.txt | wc -l
#fgrep '404 Not Found' ./collect-logs.txt | wc -l
#fgrep 'fake image result for image not found' ./collect-logs.txt | wc -l

echo "- Distribution:"
fgrep 'media entries' collect-logs.txt | cut -d' ' -f4- | sort | uniq -c

echo ""
echo "Full archive:"

echo -n "- Known URLs:     "
cat all-urls.txt | wc -l

echo -n "- 404/missing:    "
( ls errors.albums ; ls errors.media ) | wc -l

echo -n "- Saved images:   "
ls images | wc -l

echo -n "- Saved metadata: "
( ls metadata.albums ; ls metadata.media ) | wc -l
